import requests
from bs4 import BeautifulSoup
import boto3
import datetime as dt

#Obtenemos el año, mes y el dia
today = dt.date.today()
year=today.year
mes=today.month
dia=today.day

## Se lee las respectivas paginas
pagElTiempo = requests.get("https://www.eltiempo.com/")
pagElEspectador = requests.get("https://www.elespectador.com/")

## Se crean los archivos html para subir a s3
htmlElTiempo = open('paginaElTiempo.html','w')
htmlElEspectador = open('paginaElEspectador.html','w')

htmlElTiempo.write(str(pagElTiempo))
htmlElEspectador.write(str(pagElEspectador))

htmlElTiempo.close()
htmlElEspectador.close()


s3 = boto3.client('s3')
response = s3.upload_file('paginaElTiempo.html', "bigdatacorte1",'headlines/raw/periodico=ElTiempo/year='+str(year)+'/month='+str(mes)+'/day='+str(dia)+'/paginaElTiempo.html')
response = s3.upload_file('paginaElEspectador.html', "bigdatacorte1",'headlines/raw/periodico=ElEspectador/year='+str(year)+'/month='+str(mes)+'/day='+str(dia)+'/paginaElEspectador.html')

