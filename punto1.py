# Importacions necesarias
import pandas as pd
import numpy as pd
from pandas_datareader import DataReader
import datetime as dt
import boto3

# Crear un rango de tiempo(variales 'inicio' y 'fin').
# El inicio se requiere que sea el 1/1/2016 y el final el dia de hoy
today = dt.date.today()
inicio=today - dt.timedelta(days=1)

fin=dt.datetime.today()

# Se obtienen las acciones deseadas
accionesAVHOQ=['AVHOQ']
accionesEC=['EC']
accionesAVAL=['AVAL']
accionesCMTOY=['CMTOY']


# Se leen los datos desde Yahoo y se asigna a un dataframe llamado 'Stocks'Nombre compañia''
StocksAVHOQ=DataReader(accionesAVHOQ,"yahoo",start=inicio,end=fin)
StocksEC=DataReader(accionesEC,"yahoo",start=inicio,end=fin)
StocksAVAL=DataReader(accionesAVAL,"yahoo",start=inicio,end=fin)
StocksCMTOY=DataReader(accionesCMTOY,"yahoo",start=inicio,end=fin)

# Se obtiene el registro del dia actual
registrosAVHOQ=StocksAVHOQ.tail(1)
registrosEC=StocksEC.tail(1)
registrosAVAL=StocksAVAL.tail(1)
registrosCMTOY=StocksCMTOY.tail(1)

#Obtenemos el año, mes y el dia
año=registrosAVAL.index.year
mes=registrosAVAL.index.month
day=registrosAVAL.index.day

#Organizamos el formato
registrosAVHOQ.columns=['Adj Close'	,'Close',	'High'	,'Low',	'Open',	'Volume']
registrosEC.columns=['Adj Close'	,'Close',	'High'	,'Low',	'Open',	'Volume']
registrosAVAL.columns=['Adj Close'	,'Close',	'High'	,'Low',	'Open',	'Volume']
registrosCMTOY.columns=['Adj Close'	,'Close',	'High'	,'Low',	'Open',	'Volume']

#Generamos los csv, que quedaran en la misma carpeta
registrosAVHOQ.to_csv('registrosAVHOQ_'+str(mes[0])+'_'+str(day[0])+'.csv')
registrosEC.to_csv('registrosEC_'+str(mes[0])+'_'+str(day[0])+'.csv')
registrosAVAL.to_csv('registrosAVAL_'+str(mes[0])+'_'+str(day[0])+'.csv')
registrosCMTOY.to_csv('registrosCMTOY_'+str(mes[0])+'_'+str(day[0])+'.csv')
# se añade a S3
s3 = boto3.client('s3')
response = s3.upload_file('registrosAVHOQ_'+str(mes[0])+'_'+str(day[0])+'.csv', "bigdatacorte1","stocks/company=AVHOQ/year="+ str(año[0])+"/month="+str(mes[0])+"/day="+str(day[0])+"/registrosAVHOQ.csv")
response = s3.upload_file('registrosEC_'+str(mes[0])+'_'+str(day[0])+'.csv', "bigdatacorte1","stocks/company=EC/year="+ str(año[0])+"/month="+str(mes[0])+"/day="+str(day[0])+"/registrosEC.csv")
response = s3.upload_file('registrosAVAL_'+str(mes[0])+'_'+str(day[0])+'.csv', "bigdatacorte1","stocks/company=AVAL/year="+ str(año[0])+"/month="+str(mes[0])+"/day="+str(day[0])+"/registrosAVAL.csv")
response = s3.upload_file('registrosCMTOY_'+str(mes[0])+'_'+str(day[0])+'.csv', "bigdatacorte1","stocks/company=CMTOY/year="+ str(año[0])+"/month="+str(mes[0])+"/day="+str(day[0])+"/registrosCMTOY.csv")

# Se realiza una funcion que automatiza el query para realizar las particiones de la tabla
athena = boto3.client('athena', region_name='us-east-1')
def iteracion_Alter(name): 
  # Se crean los parametros 
  params = {
      'region': 'us-east-1',
      'database': 'bigdataparcial1',
      'bucket': 'bigdatacorte1',
      'path': 'temp/athena/output',
      'query': "ALTER TABLE stocks add partition (company='"+name+"',year="+str(año[0])+",month="+str(mes[0])+",day="+str(day[0])+");"
    }
  
  # Se ejecuta el query
  response_query_execution_id = athena.start_query_execution(
    QueryString = params['query'],
    QueryExecutionContext = {
      'Database' : params['database']
    },
    ResultConfiguration = {
      'OutputLocation' : 's3://' + params['bucket'] + '/' + params['path']
    }
  )


# Se ejecuta funcion por cada nombre de compañia
iteracion_Alter("AVHOQ")
iteracion_Alter("AVAL")
iteracion_Alter("EC")
iteracion_Alter("CMTOY")
